package model;

public class Person {

    private String name;

    private boolean employed;

    private int age;

    private float salary;

    private Address address;

    private Company job;

    public Person(String name, boolean employed, int age, float salary, Address address, Company job) {
        this.name = name;
        this.employed = employed;
        this.age = age;
        this.salary = salary;
        this.address = address;
        this.job = job;
    }
}
