package model;

public class Address {

    private String street;

    private short apartment;

    public Address(String street, short apartment) {
        this.street = street;
        this.apartment = apartment;
    }
}
